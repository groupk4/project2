#include <stdio.h>
#include <stdlib.h>

struct Node
{
        int number;
        struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
        struct Node *head = NULL;
        int choice, data, value, key;

        while (1)
        {
                printf("Linked Lists\n");
                printf("1. Print List\n");
                printf("2. Append\n");
                printf("3. Prepend\n");
                printf("4. Delete a certain element by value\n");
                printf("6.insert at a certain position \n");
                printf("7.delete element at a certain position \n");
                printf("5. Exit\n");
                printf("Enter your choice: ");
                scanf("%d", &choice);

                switch (choice)
                {
                case 1:
                        printList(head);
                        break;
                case 2:
                        printf("Enter the value to append: ");
                        scanf("%d", &data);
                        append(&head, data);
                        break;
                case 3:
                        printf("Enter a value to prepend: ");
                        scanf("%d", &data);
                        prepend(&head, data);
                        break;
                case 4:
                        printf("Enter the value to delete: ");
                        scanf("%d", &data);
                        deleteByValue(&head, data);
                        break;
                case 5:
                        return 1;
            	case 6:
            			printf("enter the value: ");
            			scanf("%d", &value);
            			printf("enter the position: ");
            			scanf("%d", &key);
            			insertAfterKey(&head,key,value);
            			 break;
            	case 7:
            			printf("enter the position: ");
            			scanf("%d", &key);
            			deleteByKey(&head,key);
            			 break;
            			
                default:
                        printf("Invalid choice ....try again ");
                        break;
                }
        }

        return 0;
}

struct Node *createNode(int num)
{
        struct Node *newNode = (struct Node *)malloc(sizeof(struct Node ));
        newNode->number = num;
        newNode->next = NULL;
        return newNode;
}

void printList(struct Node *head)
{
        struct Node *temp = head;
        printf("[ ");
        while (temp != NULL)
        {
                printf("%d, ", temp->number);
                temp = temp->next;
        }
        printf(" ]\n");
}

void append(struct Node **head, int num)
{
        struct Node *newNode = createNode(num);
        if (*head == NULL)
        {
                *head = newNode;
                return;
        }
        struct Node *temp = *head;
        while (temp->next != NULL)
        {
                temp = temp->next;
        }
        temp->next = newNode;
}

void prepend(struct Node **head, int num)
{
        struct Node *newNode = createNode(num);
        newNode->next = *head;
        *head = newNode;
}

void deleteByKey(struct Node **head, int key)
{
	
        struct Node *temp = *head, *prev = *head;
        // taking the key as a position of a node
        if(*head == NULL){
        	printf("the list is already empty");
		}
        if(key == 1){
        	*head = temp->next;
            free(temp);
            temp=NULL;      	
		}
		while(key != 1 ){
			prev = temp;
			temp = temp->next;
			key--;
		}
		prev->next =temp->next;
		free(temp);
		temp=NULL;
}



void deleteByValue(struct Node **head, int value)
{
        struct Node *temp = *head, *prev = NULL;
        if (temp != NULL && temp->number == value)
        {
                *head = temp->next;
                free(temp);
                return;
        }
        while (temp != NULL && temp->number != value)
        {
                prev = temp;
                temp = temp->next;
        }
        prev->next = temp->next;
        free(temp);
}

void insertAfterKey(struct Node **head, int key, int value)
{	//taking the key as the position you want to insert to given its not the first position..since you have function in place to handle that
        struct Node *temp = *head;
        if(key <=1){
        	printf("please use prepend to insert to position 1 \n");
		}
		else{
        key--;
        while(key !=1 ){
        	temp = temp->next;
        	key--;
        	return;
		}
        struct Node *newNode = createNode(value);
        newNode->next = temp->next;
        temp->next = newNode;
		}
}


void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
        struct Node *temp = *head;
        while (temp != NULL && temp->number != searchValue)
        {
                temp = temp->next;
        }
        struct Node *newNode = createNode(newValue);
        newNode->next = temp->next;
        temp->next = newNode;
}


